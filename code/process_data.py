import pandas as pd
import numpy as np

def process(vocab_list,df):
    word_to_idx = {word: i for i, word in enumerate(vocab_list)}
    print(word_to_idx)

    idx_to_word = {word_to_idx[word]: word for word in word_to_idx}

    l = []
    total = []

    for sentence in df['Sentence']:
        #for i in sentence.strip().split(' '):

        for i in sentence.strip().split(' '):
            if i in word_to_idx:
                l.append(word_to_idx[i])
            elif i == '':
                pass
        total.append(l)
        l = []

    df = pd.concat([df,pd.DataFrame({'Indices': total})],axis = 1)

    df = df[df['Indices'].map(lambda d: len(d)) > 0]
    return df, word_to_idx
