import torch
import torch.nn as nn
from torch.autograd import Variable
from torch.nn import functional as F



class LSTMTagger(nn.Module):
    def __init__(self,embedding_dim,hidden_dim,vocab_size, weight):
        super(LSTMTagger, self).__init__()
        self.hidden_dim = hidden_dim
        self.weight = weight
        self.word_embedding = nn.Embedding.from_pretrained(self.weight)
        self.lstm = nn.LSTM(embedding_dim,hidden_dim)
        self.hidden = self.init_hidden()



    def init_hidden(self):
        return (torch.zeros(1, 1, self.hidden_dim),
                torch.zeros(1, 1, self.hidden_dim))

    def forward(self, sentence):
        embeds = self.word_embeddings(sentence)
        lstm_out, self.hidden = self.lstm(
            embeds.view(len(sentence), 1, -1), self.hidden)
        # tag_space = self.hidden2tag(lstm_out.view(len(sentence), -1))
        # tag_scores = F.log_softmax(tag_space, dim=1)
        return print('training finished')


# class Classifier_Model(nn.Module):
#     def __init__(self, nb_layers, embedding_dim, hidden_dim, word_to_idx, tagset_size):
#         super().__init__()
#
#         # self.vocab = word_to_idx
#         self.nb_layers = nb_layers
#         self.embedding_dim = embedding_dim
#         self.hidden_dim = hidden_dim
#         self.batch_size = batch_size
#         self.lstm
#         self.__build_model()
#
#     def __build_model(self):
#         # nb_vocab_words = len(self.vocab)
#         # padding_idx = self.vocab['<PAD>']
#         self.word_embedding = nn.Embedding.from_pretrained(weight)
#         self.LSTM(input_size = self.embedding_dim,
#                   hidden_size = self.hidden_dim,
#                   num_layers = self.nb_layers,
#                   batch_first = True)
#
#     def init_hidden(self):
#
#         # (number_of_layers * batch_size * hidden_dim)
#         hidden_a = Variable(torch.randn(self.nb_layers, self.batch_size,self.hidden_dim))
#         hidden_b = Variable(torch.randn(self.nb_layers,self.batch_size,self.hidden_dim))
#
#         return hidden_a, hidden_b
#
#
#     def forward(self,X,X_length):
#         self.hidden = self.init_hidden()
#
#         batch_size, seq_len, _ = X.size()
#         # embed the input
#         X = self.word_embedding(X)
#
#         # run LSTM
#         X, self.hidden = self.lstm(X, self.hidden)
