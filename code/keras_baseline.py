from __future__ import print_function
import numpy as np
from time import time
from keras.callbacks import TensorBoard

from keras.preprocessing import sequence
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Embedding
from keras.layers import GlobalAveragePooling1D
from keras.datasets import imdb
import pandas as pd
from sklearn.model_selection import train_test_split

def tsplit(string, delimiters):
    """Behaves str.split but supports multiple delimiters."""

    delimiters = tuple(delimiters)
    stack = [string, ]

    for delimiter in delimiters:
        for i, substring in enumerate(stack):
            substack = substring.split(delimiter)
            stack.pop(i)
            for j, _substring in enumerate(substack):
                stack.insert(i + j, _substring)

    return stack

def create_ngram_set(input_list, ngram_value=2):
    return set(zip(*[input_list[i:] for i in range(ngram_value)]))


def add_ngram(sequences, token_indice, ngram_range=2):
    new_sequences = []
    for input_list in sequences:
        new_list = input_list[:]
        for ngram_value in range(2, ngram_range + 1):
            for i in range(len(new_list) - ngram_value + 1):
                ngram = tuple(new_list[i:i + ngram_value])
                if ngram in token_indice:
                    new_list.append(token_indice[ngram])
        new_sequences.append(new_list)

    return new_sequences


ngram_range = 3
max_features = 20000
maxlen = 400
batch_size = 32
embedding_dims = 50
epochs = 5

df = pd.read_table('/Users/rupalisinha/TransferNN/venv/Output_files/output_concepts.tsv', sep="\t", header=None)
df.columns = ['sent_id', 'sent', 'concepts']
df_str = pd.read_table('/Users/rupalisinha/TransferNN/venv/Output_files/sen_tags.tsv', sep="\t", header=None)
df_str.columns = ['sent_id', 'sent', 'concepts']


concept_list=[]
for i in df_str['concepts']:
    x = tsplit(i,(' ','-'))
    for item in x:
        if len(item) >1:
            if item not in concept_list:
                concept_list.append(item)




big_l=[]
l=[]
for i in df['sent']:
    x = tsplit(i,('[',']',',',' '))
    for item in x:
        if item == '':
            pass
        else:
            l.append(int(item))
    big_l.append(l)
    l=[]
sent_dict = {'sent':big_l}
big_l=[]
for i in df['concepts']:
    x = tsplit(i,('[',']',',',' '))
    for item in x:
        if item == '':
            pass
        else:
            l.append(int(item))
    big_l.append(l)
    l=[]
con_dict = {'concepts':big_l}
df_sent = pd.DataFrame.from_dict(sent_dict)
df_con = pd.DataFrame.from_dict(con_dict)
df=df.drop(['sent','concepts'], axis=1)
df=pd.concat([df,df_sent,df_con], axis=1)

s=[]
d1=''
for i in df_str['concepts']:
    for val in tsplit(i,(' ','-')):
        if len(val) >1:
            d1 += val + ' '
    s.append(d1.rstrip(' '))
    d1=''



from sklearn.feature_extraction.text import CountVectorizer

vectorizer = CountVectorizer(binary=True)
data = vectorizer.fit_transform(concept_list)
newData = (vectorizer.transform(s)).toarray()


# train validation test split of the dataframe
x,x_test, y, y_test = train_test_split(df['sent'],newData,test_size=0.10,train_size=0.90)
x_train, x_val, y_train, y_val = train_test_split(x,y, test_size=0.11,train_size=0.89)

print(len(x_train), 'train sequences')
print(len(x_val), 'validation sequences')
print(len(x_test), 'test sequences')

print('Max train sequence length: {}'.format(max(list(map(len, x_train)))))
print('Max validation sequence length: {}'.format(max(list(map(len,x_val)))))
print('Max test sequence length: {}'.format(max(list(map(len,x_test)))))




if ngram_range > 1:
    print('Adding {}-gram features'.format(ngram_range))
    # Create set of unique n-gram from the training set.
    ngram_set = set()
    for input_list in x_train:
        for i in range(2, ngram_range + 1):
            set_of_ngram = create_ngram_set(input_list, ngram_value=i)
            ngram_set.update(set_of_ngram)

    start_index = max_features + 1
    token_indice = {v: k + start_index for k, v in enumerate(ngram_set)}
    indice_token = {token_indice[k]: k for k in token_indice}
    max_features = np.max(list(indice_token.keys())) + 1

    x_train = add_ngram(x_train, token_indice, ngram_range)
    x_val = add_ngram(x_val, token_indice, ngram_range)
    x_test = add_ngram(x_test, token_indice, ngram_range)

    x_train_max = max(list(map(len, x_train)))
    x_val_max = max(list(map(len, x_val)))
    x_test_max = max(list(map(len, x_test)))

    y_train_max = max(list(map(len, y_train)))
    y_val_max = max(list(map(len, y_train)))
    y_test_max = max(list(map(len, y_train)))

    print('Max x_train sequence length: {}'.format(max(list(map(len, x_train)))))
    print('Max x_val sequence length: {}'.format(max(list(map(len, x_val)))))
    print('Max x_test sequence length: {}'.format(max(list(map(len, x_test)))))
    print('\n')

    print('Max y_train sequence length: {}'.format(max(list(map(len, y_train)))))
    print('Max y_val sequence length: {}'.format(max(list(map(len, y_val)))))
    print('Max y_test sequence length: {}'.format(max(list(map(len, y_test)))))
    print('\n')

print('Pad sequences (samples x time)')
print('\n')
x_train = sequence.pad_sequences(x_train, maxlen=x_train_max)
x_val = sequence.pad_sequences(x_val, maxlen=x_train_max)
x_test = sequence.pad_sequences(x_test, maxlen=x_train_max)

y_train = sequence.pad_sequences(y_train, maxlen=y_train_max)
y_val = sequence.pad_sequences(y_val, maxlen=y_train_max)
y_test = sequence.pad_sequences(y_test, maxlen=y_train_max)
print('x_train shape:', x_train.shape)
print('x_val shape:', x_val.shape)
print('x_test shape:', x_test.shape)
print('\n')
print('y_train shape:', y_train.shape)
print('y_val shape:', y_val.shape)
print('y_test shape:', y_test.shape)
print('\n')

print('Build model...')
model = Sequential()
model.add(Embedding(max_features,
                    embedding_dims,
                    input_length=x_train_max))

model.add(GlobalAveragePooling1D())
model.add(Dense(22, activation='sigmoid'))
model.compile(loss='binary_crossentropy',
              optimizer='adam',
              metrics=['accuracy'])
tensorboard = TensorBoard(log_dir="logs/{}".format(time()))
model.fit(x_train, y_train,
          batch_size=batch_size,
          epochs=epochs,
          validation_data=(x_val, y_val),
          callbacks=[tensorboard])

# score = model.evaluate(x_test,y_test)
# print(score)
# print(model.metrics_names)

