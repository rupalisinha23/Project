import pandas as pd
import sys
import glob
import os


def findFiles(path):
    return glob.glob(path)


def tsplit(string, delimiters):
    """Behaves str.split but supports multiple delimiters."""

    delimiters = tuple(delimiters)
    stack = [string, ]

    for delimiter in delimiters:
        for i, substring in enumerate(stack):
            substack = substring.split(delimiter)
            stack.pop(i)
            for j, _substring in enumerate(substack):
                stack.insert(i + j, _substring)

    return stack


def chunker(seq, size):
    return (seq[pos:pos + size] for pos in range(0, len(seq), size))


def concept_parse_to_df(path):
    # change to the path where the data is located
    os.chdir(path)
    data = ''


    list_files = findFiles('*.conll')


    for file_name in list_files:
        with open(file_name,'r',encoding='utf-8') as file:
            data += file.read()

    data = data.split('\n\n')

    sentence1 = []
    concepts1 = []

    for item in data:
        word1 = ''
        concept1 = ''

        for group in chunker(tsplit(item, ('\t','\n')), 3):
            for idx, val in enumerate(group):
                if idx == 0:
                    word1 += val + ' '
                elif idx ==2:
                    concept1 += val.strip('O') + ' '
                else:
                    pass


        sentence1.append(word1)
        concepts1.append(concept1)


    table = pd.concat([pd.DataFrame(concepts1, columns = ['Concept']),pd.DataFrame(sentence1, \
                                                                                   columns = ['Sentence'])],axis=1)

    # (4515,2) : size of the dataframe
    print('4515 rows written into the dataframe!')
    return table
