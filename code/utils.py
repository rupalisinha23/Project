import numpy as np

def create_input_padded_X(df,word_to_idx):
    X = [item for item in df['Indices']]
    print(X)
    # get the length of each sentence
    X_lengths = [len(sentence) for sentence in X]

    # create an empty matrix with padding tokens
    pad_token = word_to_idx['<PAD>']
    print(pad_token)
    longest_sent = max(X_lengths)
    batch_size = len(X)
    padded_X = np.ones((batch_size, longest_sent)) * pad_token
    # copy over the actual sequences
    for i, x_len in enumerate(X_lengths):
        sequence = X[i]
        padded_X[i, 0:x_len] = sequence[:x_len]
    return padded_X