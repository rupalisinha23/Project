import pandas as pd
import numpy as np

def vocabulary(table):
    for i in table['Sentence']:
        with open('vocab_file.txt','a+') as f:
            f.writelines(' '.join(i.split(' ')) + u'\n')
    return print('Input file for fasttext is ready. Please use it to run the word representations.')


def extract_ft_vocab(path):
    vocab_conll_list = ['<PAD>']

    vector = [0] * 100
    embeddings = [np.asarray(vector)]

    with open(path,'r',encoding='utf-8') as vecs:
        next(vecs)  # skip count header
        for vec in vecs:
            data = vec.strip().split(' ')


            if data[0] == '</s>' or data[0] in ',()<>:-!?/' or data[0] == '-->':
                pass
            elif data[0].isdigit():
                pass
            else:
                vocab_conll_list.append(data[0])
                embeddings.append(np.fromstring(' '.join(data[1:]), sep = ' '))

    return vocab_conll_list, np.concatenate(embeddings, axis=0)
