from sklearn.feature_extraction.text import CountVectorizer
import numpy as np


def neighborhood(iterable):
    iterator = iter(iterable)
    prev_item = None
    current_item = next(iterator)  # throws StopIteration if empty.
    for next_item in iterator:
        yield (prev_item, current_item, next_item)
        prev_item = current_item
        current_item = next_item
    yield (prev_item, current_item, None)


def doc_features(word2idx):

    tiny_vocab=[]
    for key, value in word2idx.items():
        tiny_vocab.append(key)
    wiki=[]
    f = open('/Users/rupalisinha/Desktop/dewiki_small.txt','r', encoding='utf-8-sig')
    for w in f.readlines():
        if w == '\n' or w.startswith('==') or w.startswith('==='):
            pass
        else:
            wiki.append(w.strip('\n'))


    wikipedia = []
    data = ''
    for prev, item, next in neighborhood(wiki):
        data+=item
        if next is not None:
            if next.startswith('='):
                wikipedia.append(data)
                data = ''



    count_vectorizer = CountVectorizer(binary='true')
    data = count_vectorizer.fit_transform(tiny_vocab)
    newData = count_vectorizer.transform(wikipedia) # change here for your docs


    x = newData.sum(axis=1)

    wikipedia_de = []
    for idx, value in enumerate(x):
        if int(value) > 100:
            wikipedia_de.append(wikipedia[idx])
        else:
            pass

    return wikipedia_de

