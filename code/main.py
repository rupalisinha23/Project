import os
import sys
import glob
from nltk import sent_tokenize

from textblob_de import TextBlobDE
import pandas as pd
import concepts_parser as cp
import unique_vocab as uv
import process_data as pd
from torch.autograd import Variable
import torch
from torch import nn
import relevant_docs as rd
import create_embedding as ce
import model

if __name__ == '__main__':
    path = '/Users/rupalisinha/Desktop/BIG_MEDILYTICS/ner_conll/'

    # function for concept parser
    table = cp.concept_parse_to_df(path)

    # function for creating unique vocabulary list
    # this function can also be performed from outside the code flow
    # output file --> vocab_conll.vec
    # uv.vocabulary(table)

    # function to extract the vocabulary words from the .connl text files.
    file_path = r'/Users/rupalisinha/Desktop/thesis_home/project/fastText/output_fast_tiny.vec'

    vocab_conll_list, embedding_matrix = uv.extract_ft_vocab(file_path)
    print('There are {} unique words in the data.'.format(len(vocab_conll_list)))

    # this calls all the relevant functions from process_data.py
    df, word2idx = pd.process(vocab_conll_list, table)
    print(len(word2idx))
    #
    # embedding layer
    padded_X, weight = ce.create_embedding_word2vec(df,word2idx, embedding_matrix)


    EMBEDDING_DIM = 100
    HIDDEN_DIM = 61
    mod = model.LSTMTagger(EMBEDDING_DIM,HIDDEN_DIM, len(word2idx), weight)
    print(mod)
    #### Embedding example ########
    # for i in df['Indices']:
    #     print(i)
    #     i_long = Variable(torch.LongTensor([i]))
    #     print(i_long)
    #
    #     embed = nn.Embedding(len(word2idx),100)
    #     print(embed(i_long))
    #     break

    ###################################

    # this is a list of documents having more than 100 medical words of the tiny corpus
    #wikipedia_de = rd.doc_features(word2idx)





