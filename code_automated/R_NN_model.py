import torch
import torch.nn as nn
from torch.autograd import Variable
from torch.nn import functional as F


class LSTMTagger(nn.Module):
    def __init__(self,embedding_dim,hidden_dim,vocab_size, weight,tagset_size):
        super(LSTMTagger, self).__init__()
        self.hidden_dim = hidden_dim
        print('self.hidden_dim: ', self.hidden_dim)
        self.weight = weight
        self.word_embedding = nn.Embedding.from_pretrained(self.weight)
        self.lstm = nn.LSTM(embedding_dim,hidden_dim, batch_first=True)
        self.hidden2tag = nn.Linear(hidden_dim,tagset_size)
        self.hidden = self.init_hidden()



    def init_hidden(self):
        return (torch.zeros(1, 1, self.hidden_dim),
                torch.zeros(1, 1, self.hidden_dim))

    def forward(self, sentence):
        embeds = self.word_embedding(sentence) # emeds shape [1,61,100]
        lstm_out, self.hidden = self.lstm(embeds, self.hidden) # shape of lstm_out[1,61,61]
        lstm_out = lstm_out.view(-1,lstm_out.shape[2])
        tag_space = self.hidden2tag(lstm_out)
        tag_scores = F.sigmoid(tag_space)  # shape of [61,23]
        print(tag_scores.max(dim=0)[0])
        print('training finished')
        return tag_scores.max(dim=0)[0]
