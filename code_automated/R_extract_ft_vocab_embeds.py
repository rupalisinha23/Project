import numpy as np


def extract_ft_vocab(path):
    vocab_conll_list = ['<PAD>']

    vector = [0] * 100
    embeddings = [np.asarray(vector)]

    with open(path,'r',encoding='utf-8') as vecs:
        next(vecs)  # skip count header
        for vec in vecs:
            data = vec.strip().split(' ')


            if data[0] == '</s>' or data[0] in ',()<>:-!?/' or data[0] == '-->':
                pass
            elif data[0].isdigit():
                pass
            else:
                vocab_conll_list.append(data[0])
                embeddings.append(np.fromstring(' '.join(data[1:]), sep = ' '))
    #print('There are {} unique words in medical corpus.'.format(len(vocab_conll_list)))

    return vocab_conll_list, np.concatenate(embeddings, axis=0)