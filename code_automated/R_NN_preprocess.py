import sys
import numpy as np
import torch
from torch import nn
from torch.autograd import Variable
import torch.optim as optim
import R_NN_utils as util
import R_NN_model as model


def tsplit(string, delimiters):
    """Behaves str.split but supports multiple delimiters."""

    delimiters = tuple(delimiters)
    stack = [string, ]

    for delimiter in delimiters:
        for i, substring in enumerate(stack):
            substack = substring.split(delimiter)
            stack.pop(i)
            for j, _substring in enumerate(substack):
                stack.insert(i + j, _substring)

    return stack


def process_from_tsv(path):
    f = open(path, 'r', encoding='utf-8').readlines()

    sentence = []
    concepts = []

    for item in f:
        for n in [1, 2]:
            i = item.strip('\n').split('\t')[n]
            x = ''.join(tsplit(i, ('[', ']', ' '))).split(',')
            x = list(map(int, x))
            if n == 1:
                sentence.append(x)
            else:
                concepts.append(x)
    max_len_sen = max([len(val) for val in sentence])
    max_len_concepts = len(set([j for i in concepts for j in i]))
    unique_concepts = set([j for i in concepts for j in i])

    return sentence, concepts, max_len_concepts, max_len_sen, unique_concepts

def extract_ft_vocab(path):
    vocab_conll_list = ['<PAD>']

    vector = [0] * 100
    embeddings = [np.asarray(vector)]

    with open(path,'r',encoding='utf-8') as vecs:
        next(vecs)  # skip count header
        for vec in vecs:
            data = vec.strip().split(' ')


            if data[0] == '</s>' or data[0] in ',()<>:-!?/' or data[0] == '-->':
                pass
            elif data[0].isdigit():
                pass
            else:
                vocab_conll_list.append(data[0])
                embeddings.append(np.fromstring(' '.join(data[1:]), sep = ' '))

    return vocab_conll_list, np.concatenate(embeddings, axis=0)


def create_index(vocab_conll):
    # gather the vocab of concepts to create con2idx
    con2idx = {'<PAD>':0,'<UNK>':1}
    count = 2
    with open('/Users/rupalisinha/TransferNN/venv/Output_files/sen_tags.tsv', 'r+') as f:
        data = f.readlines()
    for item in data:
        temp = (item.strip('\n').split('\t'))[2].split(' ')
        for val in temp:
            if len(val.split('-')) > 1:
                if val.split('-')[1] not in con2idx:
                    con2idx[val.split('-')[1]] = count
                    count +=1
            else:
                pass

    d = ""
    concept_list = []  # for making one hot vector for target
    for item in data:
        temp = item.strip('\n').split('\t')[2]
        split_list = tsplit(temp,('-',' '))
        for i in split_list:
            if len(i) > 1:
                d +=i + ' '
        concept_list.append(d.rstrip(' '))
        d = ''


    word2idx = {}
    for idx, val in enumerate(vocab_conll):
        if val not in word2idx:
            word2idx[val] = idx
    return word2idx, con2idx, concept_list





if __name__=='__main__':
    path = sys.argv[1]
    path_vec = sys.argv[2]
    sentence, concepts, max_len_concepts, max_len_sen, unique_concepts = process_from_tsv(path)

    vocab_conll, embedding_matrix = extract_ft_vocab(path_vec)
    embedding_matrix = torch.FloatTensor(embedding_matrix)
    # print(embedding_matrix.shape) # shape torch.Size([211900])
    weight = embedding_matrix.view(len(vocab_conll), 100)
    # print(weight.shape)  # shape torch.Size([2119,100])

    # create word2idx
    word2idx, con2idx, concept_list = create_index(vocab_conll)


    # padding sentences ----> ! BATCH SIZE OF VOCABULARY SIZE
    padded_X = util.create_input_padded_X(sentence,word2idx, max_len_sen)

    EMBEDDING_DIM = 100
    HIDDEN_DIM = 61
    mod = model.LSTMTagger(EMBEDDING_DIM, HIDDEN_DIM, len(word2idx), weight, len(con2idx))
    print(mod)
    loss_function = nn.BCELoss()
    optimizer = optim.SGD(mod.parameters(), lr=0.1)

    from sklearn.feature_extraction.text import CountVectorizer

    vectorizer = CountVectorizer(binary=True)
    data = vectorizer.fit_transform([key for key, val in con2idx.items()])
    newData = (vectorizer.transform(concept_list)).toarray()
    newData = Variable(torch.FloatTensor(newData))


    # count_vectorizer = CountVectorizer(binary='true')
    # data = count_vectorizer.fit_transform(tiny_vocab)
    # newData = count_vectorizer.transform(wikipedia)




    for epoch in range(2):
        for sentence, tag in zip(padded_X, newData):
            #tag_out = torch.LongTensor(tags)
            #tag_out = Variable(torch.FloatTensor([[0]*23]* 1))
            tag_out = tag
            print(tag_out.shape)
            print('sentence: ',sentence)
            sen_in = Variable(torch.LongTensor(sentence))
            sen_in = sen_in.view(1,max_len_sen)

            mod.zero_grad()
            mod.hidden = mod.init_hidden()


            tag_scores = mod(sen_in)

            loss = loss_function(tag_scores,tag_out)
            print(loss)
            loss.backward()
            optimizer.step()

