import sys
import os
import glob
import pandas as pd


import R_extract_ft_vocab_embeds as ef
import R_process_med_output_tsv as pm


def findFiles(path):
    return glob.glob(path)


def tsplit(string, delimiters):
    """Behaves str.split but supports multiple delimiters."""

    delimiters = tuple(delimiters)
    stack = [string, ]

    for delimiter in delimiters:
        for i, substring in enumerate(stack):
            substack = substring.split(delimiter)
            stack.pop(i)
            for j, _substring in enumerate(substack):
                stack.insert(i + j, _substring)

    return stack


def chunker(seq, size):
    return (seq[pos:pos + size] for pos in range(0, len(seq), size))


def concept_parse_to_df(path):
    # change to the path where the data is located
    os.chdir(path)
    data = ''


    list_files = findFiles('*.conll')


    for file_name in list_files:
        with open(file_name,'r',encoding='utf-8') as file:
            data += file.read()

    data = data.split('\n\n')

    sentence1 = []
    concepts1 = []

    for item in data:
        word1 = ''
        concept1 = ''

        for group in chunker(tsplit(item, ('\t','\n')), 3):
            for idx, val in enumerate(group):
                if idx == 0:
                    word1 += val + ' '
                elif idx ==2:
                    concept1 += val.strip('O') + ' '
                else:
                    pass


        sentence1.append(word1)
        concepts1.append(concept1)

    concepts1 = [" ".join(i.split()) for i in concepts1]
    for idx, v in enumerate(concepts1):
        if v == '':
            concepts1[idx] = '<UNK>'


    table = pd.concat([pd.DataFrame(concepts1, columns = ['Concept']),pd.DataFrame(sentence1, \
                                                                                   columns = ['Sentence'])],axis=1)


    # (4515,2) : size of the dataframe
    return table




if __name__=='__main__':

    conll_data_path = sys.argv[1]
    ft_med_vecfile_path = sys.argv[2]
    sen_tag_file = sys.argv[3]
    df = concept_parse_to_df(conll_data_path)
    vocab_conll_list, embedding_matrix = ef.extract_ft_vocab(ft_med_vecfile_path)

    df, word2idx, concept_to_idx = pm.process(vocab_conll_list, df)
    count = 0
    with open(sen_tag_file, 'w+') as f:
        for i,j in zip(df['Sentence'], df['Concept']):
            f.writelines('{}\t{}\t{}\n'.format(count,i,j))
            count +=1
        f.close()



