#!/usr/bin/env bash
echo "Making output directory to store appropriate files"

mkdir -p Output_files
cd Output_files

#echo "Running the preprocessing of medical data"
#python /Users/rupalisinha/TransferNN/venv/R_process_data.py /Users/rupalisinha/Desktop/BIG_MEDILYTICS/ner_conll/ /Users/rupalisinha/Desktop/thesis_home/project/fastText/output_fast_tiny.vec /Users/rupalisinha/TransferNN/venv/Output_files/sen_tags.tsv > output_concepts.tsv
#echo "tsv file is ready for medical data."

echo "Starting tsv preprocessing and training."
python /Users/rupalisinha/TransferNN/venv/R_NN_preprocess.py /Users/rupalisinha/TransferNN/venv/Output_files/output_concepts.tsv /Users/rupalisinha/Desktop/thesis_home/project/fastText/output_fast_tiny.vec
