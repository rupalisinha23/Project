import pandas as pd
import numpy as np
import sys


def process(vocab_list,df):
    word_to_idx = {word: i for i, word in enumerate(vocab_list)}

    idx_to_word = {word_to_idx[word]: word for word in word_to_idx}

    l = []
    total = []

    for sentence in df['Sentence']:
        #for i in sentence.strip().split(' '):

        for i in sentence.strip().split(' '):
            if i in word_to_idx:
                l.append(word_to_idx[i])
            elif i == '':
                pass
        total.append(l)
        l = []

    df = pd.concat([df,pd.DataFrame({'Sen Indices': total})],axis = 1)

    df = df[df['Sen Indices'].map(lambda d: len(d)) > 0]

    concepts_to_idx, df = process_concepts_tags(df)
    return df, word_to_idx, concepts_to_idx


def process_concepts_tags(df):
    l = {'<PAD>':0,'<UNK>':1}
    count = 2
    for item in df['Concept']:
        if item != '<UNK>':
            for i in item.split(' '):
                if i.split('-')[1] not in l:
                    l[i.split('-')[1]] = count
                    count +=1

    #print('There are {} concepts in the data.'.format(len(l)))
    df = update_df_for_concepts(df, l)
    return l,df


def update_df_for_concepts(df,concept_to_idx):
    c_indices = []
    c = []
    df = df.reset_index(drop=True)
    

    for item in df['Concept']:
        if item == '<UNK>':
            c.append(concept_to_idx[item])
        else:
            for i in item.split(' '):
                #if i.split('-')[1] not in concept_to_idx:
                c.append(concept_to_idx[i.split('-')[1]])
            # print('This is line {}.'.format(count))
            # print(item.split(' '))

        c_indices.append(c)
        c = []

    df = pd.concat([df, pd.DataFrame({'Concept Indices': c_indices})], axis=1)
    count=0
    for i, j in zip(df['Sen Indices'],df['Concept Indices']):
        sys.stdout.write('{}\t{}\t{}\n'.format(count,i,j))
        count +=1


    return df