import numpy as np


def create_input_padded_X(sentence, word2idx, max_len_sen):
    X = sentence

    # # get the length of each sentence
    X_lengths = [len(sentence) for sentence in X]

    # create an empty matrix with padding tokens
    pad_token = word2idx['<PAD>']

    longest_sent = max_len_sen
    batch_size = len(X)
    padded_X = np.ones((batch_size, longest_sent)) * pad_token
    # copy over the actual sequences
    for i, x_len in enumerate(X_lengths):
        sequence = X[i]
        padded_X[i, 0:x_len] = sequence[:x_len]
        print(padded_X.shape)

        break
    return padded_X

